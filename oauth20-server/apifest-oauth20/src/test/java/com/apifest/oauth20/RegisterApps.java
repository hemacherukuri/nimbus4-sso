package com.apifest.oauth20;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RegisterApps {

	public static void main(String[] args) {

		RegisterApps ra = new RegisterApps();
		ra.setUp();

		ra.registerApplicationNimbusAdmin();

		ra.registerApplicationNimbusApi();

	}

	String oauthUrl = "http://localhost:3000/oauth20";
	String registerAppUrl = "http://localhost:3000/oauth20/applications";
	String urlGetAccessToken = "http://localhost:3000/oauth20/tokens";
	String urlValidateAccessToken = "http://localhost:3000/oauth20/tokens/validate";
	String urlRevokeAccessToken = "http://localhost:3000/oauth20/tokens/revoke";

	String POST = "POST";
	String GET = "GET";

	static Properties props = null;
	static String propsFile = "nimbus4.apps.properties";

	public void setUp() {
		InputStream is = RegisterApps.class.getClassLoader().getResourceAsStream(propsFile);
		props = new Properties();

		try {
			props.load(is);
			System.out.println("loaded " + propsFile);
		} catch (IOException e) {
			System.err.println("Unable to read " + propsFile);
			e.printStackTrace();
		}

		oauthUrl = props.getProperty("oauthUrl");
		registerAppUrl = props.getProperty("registerAppUrl");
		urlGetAccessToken = props.getProperty("urlGetAccessToken");
		urlValidateAccessToken = props.getProperty("urlValidateAccessToken");
		urlRevokeAccessToken = props.getProperty("urlRevokeAccessToken");

	}

	public String getOAuthResponse(String url, String jsonReq, String requestType) {

		String retValStr = null;
		try {

			System.out.println("Url : " + url);
			System.out.println("Request data : " + jsonReq);
			System.out.println("Request type : " + requestType);

			ResponseEntity<String> retVal;
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(jsonReq, headers);

			if (requestType.equals("GET")) {
				retVal = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			} else {
				retVal = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			}

			System.out.println("Response from oauth server : " + retVal.getBody().toString());

			retValStr = retVal.getBody().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValStr;
	}

	public void registerApplicationNimbusAdmin() {

		System.out.println("--- registerApplicationNimbus4Admin ");

		String adminApplicationName = props.getProperty("adminApplicationName");
		String adminApplicationUrl = props.getProperty("adminApplicationUrl");
		String adminApplicationDescription = props.getProperty("adminApplicationDescription");

		// String applicationUrl = "http://localhost:8080/nimbus4-api";

		if (adminApplicationName != null && adminApplicationName.trim().length() > 0 && adminApplicationUrl != null
				&& adminApplicationUrl.trim().length() > 0 && adminApplicationDescription != null
				&& adminApplicationDescription.trim().length() > 0) {
			String jsonReq = "{\"name\":\"applicationName\",\"description\":\"applicationDescription\",\"scope\":\"photos email read\","
					+ "\"redirect_uri\":\"applicationUrl\",\"status\":\"1\",\"application_details\":{\"key1\":\"value\"}}";
			jsonReq = jsonReq.replace("applicationName", adminApplicationName);
			jsonReq = jsonReq.replace("applicationDescription", adminApplicationDescription);
			jsonReq = jsonReq.replace("applicationUrl", adminApplicationUrl);

			String bodyVal = getOAuthResponse(registerAppUrl, jsonReq, POST);

			System.out.println("--- registerApplicationNimbus4Admin done : " + bodyVal);

		} else {
			System.out
					.println("adminApplicationName or adminApplicationUrl or adminApplicationDescription is null or empty.");
			System.out.println("Please check the src/test/resources/nimbus4.apps.properties");

		}

	}

	public void registerApplicationNimbusApi() {

		System.out.println("--- registerApplicationNimbusApi ");

		String apiApplicationName = props.getProperty("apiApplicationName");
		String apiApplicationDescription = props.getProperty("apiApplicationDescription");
		String apiApplicationUrl = props.getProperty("apiApplicationUrl");

		if (apiApplicationName != null && apiApplicationName.trim().length() > 0 && apiApplicationDescription != null
				&& apiApplicationDescription.trim().length() > 0 && apiApplicationUrl != null
				&& apiApplicationUrl.trim().length() > 0) {

			String jsonReq = "{\"name\":\"applicationName\",\"description\":\"applicationDescription\",\"scope\":\"photos email read\","
					+ "\"redirect_uri\":\"applicationUrl\",\"status\":\"1\",\"application_details\":{\"key1\":\"value\"}}";
			jsonReq = jsonReq.replace("applicationName", apiApplicationName);
			jsonReq = jsonReq.replace("applicationDescription", apiApplicationDescription);
			jsonReq = jsonReq.replace("applicationUrl", apiApplicationUrl);

			String bodyVal = getOAuthResponse(registerAppUrl, jsonReq, POST);
			System.out.println("--- registerApplicationNimbusApi done : " + bodyVal);
		} else {
			System.out.println("apiApplicationName or apiApplicationDescription or apiApplicationUrl is null or empty");
			System.out.println("Please check the src/test/resources/nimbus4.apps.properties");
		}
	}

}
