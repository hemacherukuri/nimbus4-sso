package com.apifest.oauth20;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ApiFestTest {

	protected static Logger log = LoggerFactory.getLogger(ApiFestTest.class);

	String oauthUrl = "http://localhost:3000/oauth20";
	String registerAppUrl = "http://localhost:3000/oauth20/applications";
	String urlGetAccessToken = "http://localhost:3000/oauth20/tokens";
	String urlValidateAccessToken = "http://localhost:3000/oauth20/tokens/validate";
	String urlRevokeAccessToken = "http://localhost:3000/oauth20/tokens/revoke";

	String POST = "POST";
	String GET = "GET";

	public static void main(String... args) throws ClassNotFoundException {
		String[] classAndMethod = args[0].split("#");
		Request request = Request.method(Class.forName(classAndMethod[0]), classAndMethod[1]);

		Result result = new JUnitCore().run(request);
		System.exit(result.wasSuccessful() ? 0 : 1);
	}

	static Properties props = null;
	static String propsFile = "nimbus4.apps.properties";

	@Before
	public void setUp() {
		InputStream is = ApiFestTest.class.getClassLoader().getResourceAsStream(propsFile);
		props = new Properties();

		try {
			props.load(is);
			log.info("loaded " + propsFile);
		} catch (IOException e) {
			log.error("Unable to read " + propsFile, e);
			e.printStackTrace();
		}

		oauthUrl = props.getProperty("oauthUrl");
		registerAppUrl = props.getProperty("registerAppUrl");
		urlGetAccessToken = props.getProperty("urlGetAccessToken");
		urlValidateAccessToken = props.getProperty("urlValidateAccessToken");
		urlRevokeAccessToken = props.getProperty("urlRevokeAccessToken");

	}

	@Test
	public void registerApplicationNimbusAdmin() {

		log.info("--- registerApplicationNimbus4Admin ");

		String adminApplicationName = props.getProperty("adminApplicationName");
		String adminApplicationUrl = props.getProperty("adminApplicationUrl");
		String adminApplicationDescription = props.getProperty("adminApplicationDescription");

		// String applicationUrl = "http://localhost:8080/nimbus4-api";

		String jsonReq = "{\"name\":\"applicationName\",\"description\":\"applicationDescription\",\"scope\":\"photos email read\","
				+ "\"redirect_uri\":\"applicationUrl\",\"status\":\"1\",\"application_details\":{\"key1\":\"value\"}}";
		jsonReq = jsonReq.replace("applicationName", adminApplicationName);
		jsonReq = jsonReq.replace("applicationDescription", adminApplicationDescription);
		jsonReq = jsonReq.replace("applicationUrl", adminApplicationUrl);

		String bodyVal = getOAuthResponse(registerAppUrl, jsonReq, POST);

		log.info("--- registerApplicationNimbus4Admin done : " + bodyVal);
	}

	@Test
	public void registerApplicationNimbusApi() {

		log.info("--- registerApplicationNimbusApi ");

		String apiApplicationName = props.getProperty("apiApplicationName");
		String apiApplicationDescription = props.getProperty("apiApplicationDescription");
		String apiApplicationUrl = props.getProperty("apiApplicationUrl");

		String jsonReq = "{\"name\":\"applicationName\",\"description\":\"applicationDescription\",\"scope\":\"photos email read\","
				+ "\"redirect_uri\":\"applicationUrl\",\"status\":\"1\",\"application_details\":{\"key1\":\"value\"}}";
		jsonReq = jsonReq.replace("applicationName", apiApplicationName);
		jsonReq = jsonReq.replace("applicationDescription", apiApplicationDescription);
		jsonReq = jsonReq.replace("applicationUrl", apiApplicationUrl);

		String bodyVal = getOAuthResponse(registerAppUrl, jsonReq, POST);
		log.info("--- registerApplicationNimbusApi done : " + bodyVal);
	}

	@Test
	public void isClientRegistered() {

		String jsonReq = "";
		boolean isClientRegistered = false;
		String clientId = "c35e376ebbd0ee7443a60e9d67208213db485e68";

		try {
			JSONArray json_data1 = new JSONArray(getOAuthResponse(registerAppUrl, jsonReq, "GET"));
			JSONObject json_data = new JSONObject();

			for (int i = 0; i < json_data1.length(); i++) {
				json_data = json_data1.getJSONObject(i);
				if (json_data.getString("client_id").equals(clientId)) {
					isClientRegistered = true;
				}
			}

			if (isClientRegistered) {
				log.info("Client registered with oauth ... ");
			} else {
				log.info("Client not registered with oauth !!! ");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getAccessTokenGrantTypePassword() {

		String username = "admin@lancom.com";
		String subdomain = "lancom";
		String password = "lancom0101";
		String client_id = "25544bc481362b019cb534e8229bb2e897eee0bf";
		String client_secret = "bd463070c4f4057454b49b5bfc26616d8d5e539fc174018dfbc0dbc02e4b6224";

		String request = "grant_type=password&" + "username=" + username + "~" + subdomain + "~" + password + "&"
				+ "password=" + password + "&" + "scope=read&" + "client_id=" + client_id + "&" + "client_secret="
				+ client_secret;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<String> entity = new HttpEntity<String>(request, headers);
		ResponseEntity<String> retVal = restTemplate.exchange(urlGetAccessToken, HttpMethod.POST, entity, String.class);

		try {
			log.info("Response from OAuth server : " + retVal.getBody().toString());
			JSONObject jsonObject = new JSONObject(retVal.getBody().toString());
			log.info("Access token : " + jsonObject.get("access_token"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validateAccessToken() {

		urlValidateAccessToken = urlValidateAccessToken
				+ "?token=ef0b062d922c0e5c6e4495c992ad47ebe87888c25ee8451008e3bccd79934044";
		String jsonReq = "";
		getOAuthResponse(urlValidateAccessToken, jsonReq, GET);
	}

	@Test
	public void revokeAccessToken() {

		String jsonReq = "{" + "\"access_token\":\"ef0b062d922c0e5c6e4495c992ad47ebe87888c25ee8451008e3bccd79934044\","
				+ "\"client_id\":\"dc389706d1965e6fa56b31ffe8a2cf159bb3baf5\","
				+ "\"client_secret\":\"378b5d3f86e056089c56ef39e9e8ad5846ced7b6082de87b5d0c06073036fbed\"" + "}";

		getOAuthResponse(urlRevokeAccessToken, jsonReq, POST);
	}

	public String getOAuthResponse(String url, String jsonReq, String requestType) {

		String retValStr = null;
		try {

			log.info("Url : " + url);
			log.info("Request data : " + jsonReq);
			log.info("Request type : " + requestType);

			ResponseEntity<String> retVal;
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(jsonReq, headers);

			if (requestType.equals("GET")) {
				retVal = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			} else {
				retVal = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			}

			log.info("Response from oauth server : " + retVal.getBody().toString());

			retValStr = retVal.getBody().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValStr;
	}
}
