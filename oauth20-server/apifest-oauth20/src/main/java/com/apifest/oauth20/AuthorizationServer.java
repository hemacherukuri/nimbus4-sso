/*
 * Copyright 2013-2014, ApiFest project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.apifest.oauth20;

import com.apifest.oauth20.api.AuthenticationException;
import com.apifest.oauth20.api.ICustomGrantTypeHandler;
import com.apifest.oauth20.api.IUserAuthentication;
import com.apifest.oauth20.api.UserDetails;
import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.QueryStringEncoder;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

/**
 * Main class for authorization.
 *
 * @author Rossitsa Borissova
 */
public class AuthorizationServer {

	static final String BASIC = "Basic ";
	private static final String TOKEN_TYPE_BEARER = "Bearer";

	protected static Logger log = LoggerFactory.getLogger(AuthorizationServer.class);

	protected DBManager db = DBManagerFactory.getInstance();
	protected ScopeService scopeService = new ScopeService();

	public ClientCredentials issueClientCredentials(HttpRequest req) throws OAuthException {
		ClientCredentials creds = null;
		String content = req.getContent().toString(CharsetUtil.UTF_8);
		String contentType = req.headers().get(HttpHeaders.Names.CONTENT_TYPE);

		log.debug("content " + content);
		log.debug("contentType " + contentType);

		if (contentType != null && contentType.contains(Response.APPLICATION_JSON)) {
			ObjectMapper mapper = new ObjectMapper();
			ApplicationInfo appInfo;
			try {
				appInfo = mapper.readValue(content, ApplicationInfo.class);
				log.debug("content readValue");
				if (appInfo.valid()) {
					String[] scopeList = appInfo.getScope().split(" ");
					for (String s : scopeList) {
						// TODO: add cache for scope
						// if (db.findScope(s) == null) {
						// throw new OAuthException(Response.SCOPE_NOT_EXIST,
						// HttpResponseStatus.BAD_REQUEST);
						// }
					}
					log.debug("content readValue 1 ");

					// check client_id, client_secret passed
					if ((appInfo.getId() != null && appInfo.getId().length() > 0)
							&& (appInfo.getSecret() != null && appInfo.getSecret().length() > 0)) {
						// if a client app with this client_id already
						// registered
						log.debug("content readValue 2 ");
						if (db.findClientCredentials(appInfo.getId()) == null) {
							creds = new ClientCredentials(appInfo.getName(), appInfo.getScope(),
									appInfo.getDescription(), appInfo.getRedirectUri(), appInfo.getId(),
									appInfo.getSecret(), appInfo.getApplicationDetails());
						} else {
							throw new OAuthException(Response.ALREADY_REGISTERED_APP, HttpResponseStatus.BAD_REQUEST);
						}
					} else {
						creds = new ClientCredentials(appInfo.getName(), appInfo.getScope(), appInfo.getDescription(),
								appInfo.getRedirectUri(), appInfo.getApplicationDetails());
					}

					log.debug("content before storeClientCredentials " + creds.getStatus());

					if (creds.getStatus() == 0) {
						creds.setStatus(1);
					}

					db.storeClientCredentials(creds);

					log.debug("after before storeClientCredentials");
				} else {
					throw new OAuthException(Response.NAME_OR_SCOPE_OR_URI_IS_NULL, HttpResponseStatus.BAD_REQUEST);
				}
			}

			catch (JsonParseException e) {
				log.error("JsonParseException");
				throw new OAuthException(e, Response.CANNOT_REGISTER_APP, HttpResponseStatus.BAD_REQUEST);
			} catch (JsonMappingException e) {
				log.error("JsonMappingException");
				throw new OAuthException(e, Response.CANNOT_REGISTER_APP, HttpResponseStatus.BAD_REQUEST);
			} catch (IOException e) {
				log.error("IOException");
				throw new OAuthException(e, Response.CANNOT_REGISTER_APP, HttpResponseStatus.BAD_REQUEST);
			}
		} else {
			throw new OAuthException(Response.UNSUPPORTED_MEDIA_TYPE, HttpResponseStatus.BAD_REQUEST);
		}
		return creds;
	}

	public String issueAuthorizationCode(HttpRequest req) throws OAuthException {

		log.debug("req.getUri() : " + req.getUri());

		AuthRequest authRequest = new AuthRequest(req);

		log.debug("authRequest.getClientId() : " + authRequest.getClientId());

		if (!isActiveClientId(authRequest.getClientId())) {
			throw new OAuthException(Response.INVALID_CLIENT_ID, HttpResponseStatus.BAD_REQUEST);
		}
		authRequest.validate();

		String scope = scopeService.getValidScope(authRequest.getScope(), authRequest.getClientId());
		if (scope == null) {
			throw new OAuthException(Response.SCOPE_NOK_MESSAGE, HttpResponseStatus.BAD_REQUEST);
		}

		AuthCode authCode = new AuthCode(generateCode(), authRequest.getClientId(), authRequest.getRedirectUri(),
				authRequest.getState(), scope, authRequest.getResponseType(), authRequest.getUserId());
		log.debug("authCode: {}", authCode.getCode());
		db.storeAuthCode(authCode);

		// return redirect URI, append param code=[Authcode]
		QueryStringEncoder enc = new QueryStringEncoder(authRequest.getRedirectUri());
		enc.addParam("code", authCode.getCode());
		return enc.toString();
	}

	public AccessToken issueAccessToken(HttpRequest req) throws OAuthException {
		log.debug(" Enter AuthorizationServer[issueAccessToken] ");

		AccessToken accessToken = null;
		try {

			TokenRequest tokenRequest = new TokenRequest(req);
			tokenRequest.validate();
			// check valid client_id, client_secret and status of the client app
			// should be active
			if (!isActiveClient(tokenRequest.getClientId(), tokenRequest.getClientSecret())) {
				throw new OAuthException(Response.INVALID_CLIENT_CREDENTIALS, HttpResponseStatus.BAD_REQUEST);
			}

			if (TokenRequest.AUTHORIZATION_CODE.equals(tokenRequest.getGrantType())) {

				log.debug("Getting token by Authorization code.");

				AuthCode authCode = findAuthCode(tokenRequest);
				// TODO: REVISIT: Move client_id check to db query
				if (authCode != null) {
					if (!tokenRequest.getClientId().equals(authCode.getClientId())) {
						throw new OAuthException(Response.INVALID_CLIENT_ID, HttpResponseStatus.BAD_REQUEST);
					}
					if (authCode.getRedirectUri() != null
							&& !tokenRequest.getRedirectUri().equals(authCode.getRedirectUri())) {
						throw new OAuthException(Response.INVALID_REDIRECT_URI, HttpResponseStatus.BAD_REQUEST);
					} else {
						// invalidate the auth code
						try {
							db.updateAuthCodeValidStatus(authCode.getCode(), false);

							accessToken = new AccessToken(TOKEN_TYPE_BEARER, getExpiresIn(TokenRequest.PASSWORD,
									authCode.getScope()), authCode.getScope(), getExpiresIn(TokenRequest.REFRESH_TOKEN,
									authCode.getScope()));

							accessToken.setUserId(authCode.getUserId());
							accessToken.setClientId(authCode.getClientId());
							accessToken.setCodeId(authCode.getId());
							db.storeAccessToken(accessToken);

						} catch (Exception e) {
							log.error("Error in getting accessToken", e);
							// /e.printStackTrace();
						}
					}
				} else {
					throw new OAuthException(Response.INVALID_AUTH_CODE, HttpResponseStatus.BAD_REQUEST);
				}
			} else if (TokenRequest.REFRESH_TOKEN.equals(tokenRequest.getGrantType())) {
				log.debug("Getting token by refresh token.");
				accessToken = db.findAccessTokenByRefreshToken(tokenRequest.getRefreshToken(),
						tokenRequest.getClientId());
				if (accessToken != null) {
					if (!accessToken.refreshTokenExpired()) {
						String validScope = null;
						if (tokenRequest.getScope() != null) {
							if (scopeService.scopeAllowed(tokenRequest.getScope(), accessToken.getScope())) {
								validScope = tokenRequest.getScope();
							} else {
								throw new OAuthException(Response.SCOPE_NOK_MESSAGE, HttpResponseStatus.BAD_REQUEST);
							}
						} else {
							validScope = accessToken.getScope();
						}
						db.updateAccessTokenValidStatus(accessToken.getToken(), false);
						AccessToken newAccessToken = new AccessToken(TOKEN_TYPE_BEARER, getExpiresIn(
								TokenRequest.PASSWORD, validScope), validScope, accessToken.getRefreshToken(),
								accessToken.getRefreshExpiresIn());
						newAccessToken.setUserId(accessToken.getUserId());
						newAccessToken.setDetails(accessToken.getDetails());
						newAccessToken.setClientId(accessToken.getClientId());
						db.storeAccessToken(newAccessToken);
						db.removeAccessToken(accessToken.getToken());
						return newAccessToken;
					} else {
						db.removeAccessToken(accessToken.getToken());
						throw new OAuthException(Response.INVALID_REFRESH_TOKEN, HttpResponseStatus.BAD_REQUEST);
					}
				} else {
					throw new OAuthException(Response.INVALID_REFRESH_TOKEN, HttpResponseStatus.BAD_REQUEST);
				}
			} else if (TokenRequest.CLIENT_CREDENTIALS.equals(tokenRequest.getGrantType())) {
				log.debug("Getting token by client credentials");
				ClientCredentials clientCredentials = db.findClientCredentials(tokenRequest.getClientId());
				String scope = scopeService.getValidScopeByScope(tokenRequest.getScope(), clientCredentials.getScope());
				if (scope == null) {
					throw new OAuthException(Response.SCOPE_NOK_MESSAGE, HttpResponseStatus.BAD_REQUEST);
				}

				accessToken = new AccessToken(TOKEN_TYPE_BEARER, getExpiresIn(TokenRequest.CLIENT_CREDENTIALS, scope),
						scope, false, null);
				accessToken.setClientId(tokenRequest.getClientId());
				Map<String, String> applicationDetails = clientCredentials.getApplicationDetails();
				if ((applicationDetails != null) && (applicationDetails.size() > 0)) {
					accessToken.setDetails(applicationDetails);
				}
				db.storeAccessToken(accessToken);
			} else if (TokenRequest.PASSWORD.equals(tokenRequest.getGrantType())) {
				log.debug("Getting token by grant type password");
				String scope = scopeService.getValidScope(tokenRequest.getScope(), tokenRequest.getClientId());
				if (scope == null) {
					throw new OAuthException(Response.SCOPE_NOK_MESSAGE, HttpResponseStatus.BAD_REQUEST);
				}

				try {
					UserDetails userDetails = authenticateUser(tokenRequest.getUsername(), tokenRequest.getPassword(),
							req);
					if (userDetails != null && userDetails.getUserId() != null) {
						accessToken = new AccessToken(TOKEN_TYPE_BEARER, getExpiresIn(TokenRequest.PASSWORD, scope),
								scope, getExpiresIn(TokenRequest.REFRESH_TOKEN, scope));
						accessToken.setUserId(userDetails.getUserId());
						accessToken.setDetails(userDetails.getDetails());
						accessToken.setClientId(tokenRequest.getClientId());
						db.storeAccessToken(accessToken);
					} else {
						throw new OAuthException(Response.INVALID_USERNAME_PASSWORD, HttpResponseStatus.UNAUTHORIZED);
					}
				} catch (AuthenticationException e) {
					// in case some custom response should be returned other
					// than
					// HTTP 401
					// for instance, if the user authentication requires more
					// user
					// details as a subsequent step
					if (e.getResponse() != null) {
						String responseContent = e.getResponse().getContent().toString(CharsetUtil.UTF_8);
						throw new OAuthException(e, responseContent, e.getResponse().getStatus());
					} else {
						log.error("Cannot authenticate user", e);
						throw new OAuthException(e, Response.CANNOT_AUTHENTICATE_USER, HttpResponseStatus.UNAUTHORIZED); // NOSONAR
					}
				}
			} else if (tokenRequest.getGrantType().equals(OAuthServer.getCustomGrantType())) {
				String scope = scopeService.getValidScope(tokenRequest.getScope(), tokenRequest.getClientId());
				if (scope == null) {
					throw new OAuthException(Response.SCOPE_NOK_MESSAGE, HttpResponseStatus.BAD_REQUEST);
				}
				try {
					accessToken = new AccessToken(TOKEN_TYPE_BEARER, getExpiresIn(TokenRequest.PASSWORD, scope), scope,
							getExpiresIn(TokenRequest.REFRESH_TOKEN, scope));
					accessToken.setClientId(tokenRequest.getClientId());
					UserDetails userDetails = callCustomGrantTypeHandler(req);
					if (userDetails != null && userDetails.getUserId() != null) {
						accessToken.setUserId(userDetails.getUserId());
						accessToken.setDetails(userDetails.getDetails());
					}
					db.storeAccessToken(accessToken);
				} catch (AuthenticationException e) {
					log.error("Cannot authenticate user", e);
					throw new OAuthException(e, Response.CANNOT_AUTHENTICATE_USER, HttpResponseStatus.UNAUTHORIZED);
				}
			}

		} catch (OAuthException e) {
			log.error("------------ error in accessToken " + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			return accessToken;
		}
	}

	protected UserDetails authenticateUser(String username, String password, HttpRequest authRequest)
			throws AuthenticationException {
		UserDetails userDetails = null;
		IUserAuthentication ua;

		if (OAuthServer.getUserAuthenticationClass() != null) {
			try {
				ua = OAuthServer.getUserAuthenticationClass().newInstance();
				userDetails = ua.authenticate(username, password, authRequest);
			} catch (InstantiationException e) {
				log.error("cannot instantiate user authentication class", e);
				throw new AuthenticationException(e.getMessage());
			} catch (IllegalAccessException e) {
				log.error("cannot instantiate user authentication class", e);
				throw new AuthenticationException(e.getMessage());
			}
		} else {
			// if no specific UserAuthentication used, always returns customerId
			// - 12345
			userDetails = new UserDetails("12345", null);
		}
		return userDetails;
	}

	protected UserDetails callCustomGrantTypeHandler(HttpRequest authRequest) throws AuthenticationException {
		UserDetails userDetails = null;
		ICustomGrantTypeHandler customHandler;
		if (OAuthServer.getCustomGrantTypeHandler() != null) {
			try {
				customHandler = OAuthServer.getCustomGrantTypeHandler().newInstance();
				userDetails = customHandler.execute(authRequest);
			} catch (InstantiationException e) {
				log.error("cannot instantiate custom grant_type class", e);
				throw new AuthenticationException(e.getMessage());
			} catch (IllegalAccessException e) {
				log.error("cannot instantiate custom grant_type class", e);
				throw new AuthenticationException(e.getMessage());
			}
		}
		return userDetails;
	}

	public static String[] getBasicAuthorizationClientCredentials(HttpRequest req) {
		// extract Basic Authorization header
		String authHeader = req.headers().get(HttpHeaders.Names.AUTHORIZATION);
		String[] clientCredentials = new String[2];
		if (authHeader != null && authHeader.contains(BASIC)) {
			String value = authHeader.replace(BASIC, "");
			Base64 decoder = new Base64();
			byte[] decodedBytes = decoder.decode(value);
			String decoded = new String(decodedBytes, Charset.forName("UTF-8"));
			// client_id:client_secret - should be changed by client password
			String[] str = decoded.split(":");
			if (str.length == 2) {
				clientCredentials[0] = str[0];
				clientCredentials[1] = str[1];
			}
		}
		return clientCredentials;
	}

	protected AuthCode findAuthCode(TokenRequest tokenRequest) {
		return db.findAuthCode(tokenRequest.getCode(), tokenRequest.getRedirectUri());
	}

	public AccessToken isValidToken(String token) {
		AccessToken accessToken = db.findAccessToken(token);
		if (accessToken != null && accessToken.isValid()) {
			if (accessToken.tokenExpired()) {
				db.updateAccessTokenValidStatus(accessToken.getToken(), false);
				return null;
			}
			return accessToken;
		}
		return null;
	}

	public ApplicationInfo getApplicationInfo(String clientId) {
		ApplicationInfo appInfo = null;
		ClientCredentials creds = db.findClientCredentials(clientId);
		if (creds != null) {
			appInfo = new ApplicationInfo();
			appInfo.setName(creds.getName());
			appInfo.setDescription(creds.getDescr());
			appInfo.setId(clientId);
			appInfo.setSecret(creds.getSecret());
			appInfo.setScope(creds.getScope());
			appInfo.setRedirectUri(creds.getUri());
			appInfo.setRegistered(new Date(creds.getCreated()));
			appInfo.setStatus(creds.getStatus());
			appInfo.setApplicationDetails(creds.getApplicationDetails());
		}
		return appInfo;
	}

	protected String generateCode() {
		return AuthCode.generate();
	}

	// hemac changed added +1
	// the db entry is always stored
	// with zero see
	// http://localhost:3000/oauth20/auth-codes?response_type=code&client_id=10849457484691&client_secret=mysecret&redirect_uri=http://localhost:8100/greeting&scope=read

	protected boolean isActiveClientId(String clientId) {
		ClientCredentials creds = db.findClientCredentials(clientId);
		if (creds != null && creds.getStatus() + 1 == ClientCredentials.ACTIVE_STATUS) {
			return true;
		}
		return false;
	}

	// check only that clientId and clientSecret are valid, NOT that the status
	// is active
	protected boolean isValidClientCredentials(String clientId, String clientSecret) {
		ClientCredentials creds = db.findClientCredentials(clientId);
		if (creds != null && creds.getSecret().equals(clientSecret)) {
			return true;
		}
		return false;
	}

	protected boolean isActiveClient(String clientId, String clientSecret) {

		log.debug("Enter AuthorizationServer[isActiveClient]  : client id :  " + clientId);
		ClientCredentials creds = db.findClientCredentials(clientId);

		if (null == creds) {
			log.debug("Client not registered with the OAuth !!");
		}
		if (creds != null && creds.getSecret().equals(clientSecret)
				&& creds.getStatus() == ClientCredentials.ACTIVE_STATUS) {
			log.debug("Client " + clientId + " is active .");
			return true;
		}
		log.debug("Client " + clientId + " is inactive .");
		return false;
	}

	protected boolean isExistingClient(String clientId) {
		ClientCredentials creds = db.findClientCredentials(clientId);
		if (creds != null) {
			return true;
		}
		return false;
	}

	protected String getExpiresIn(String tokenGrantType, String scope) {

		return String.valueOf(scopeService.getExpiresIn(tokenGrantType, scope));
	}

	public boolean revokeToken(HttpRequest req) throws OAuthException {
		RevokeTokenRequest revokeRequest = new RevokeTokenRequest(req);
		revokeRequest.checkMandatoryParams();
		String clientId = revokeRequest.getClientId();
		// check valid client_id, status does not matter as token of inactive
		// client app could be revoked too
		if (!isExistingClient(clientId)) {
			throw new OAuthException(Response.INVALID_CLIENT_ID, HttpResponseStatus.BAD_REQUEST);
		}
		String token = revokeRequest.getAccessToken();
		AccessToken accessToken = db.findAccessToken(token);
		if (accessToken != null) {
			if (accessToken.tokenExpired()) {
				log.debug("access token {} is expired", token);
				return true;
			}
			if (clientId.equals(accessToken.getClientId())) {
				db.removeAccessToken(accessToken.getToken());
				log.debug("access token {} set status invalid", token);
				return true;
			} else {
				log.debug("access token {} is not obtained for that clientId {}", token, clientId);
				return false;
			}
		}
		log.debug("access token {} not found", token);
		return false;
	}

	public boolean updateClientApp(HttpRequest req, String clientId) throws OAuthException {
		String content = req.getContent().toString(CharsetUtil.UTF_8);
		String contentType = req.headers().get(HttpHeaders.Names.CONTENT_TYPE);
		if (contentType != null && contentType.contains(Response.APPLICATION_JSON)) {
			// String clientId = getBasicAuthorizationClientId(req);
			// if (clientId == null) {
			// throw new OAuthException(Response.INVALID_CLIENT_ID,
			// HttpResponseStatus.BAD_REQUEST);
			// }
			if (!isExistingClient(clientId)) {
				throw new OAuthException(Response.INVALID_CLIENT_ID, HttpResponseStatus.BAD_REQUEST);
			}
			ObjectMapper mapper = new ObjectMapper();
			ApplicationInfo appInfo;
			try {
				appInfo = mapper.readValue(content, ApplicationInfo.class);
				if (appInfo.validForUpdate()) {
					if (appInfo.getScope() != null) {
						String[] scopeList = appInfo.getScope().split(" ");
						for (String s : scopeList) {
							if (db.findScope(s) == null) {
								throw new OAuthException(Response.SCOPE_NOT_EXIST, HttpResponseStatus.BAD_REQUEST);
							}
						}
					}
					db.updateClientApp(clientId, appInfo.getScope(), appInfo.getDescription(), appInfo.getStatus(),
							appInfo.getApplicationDetails());
				} else {
					throw new OAuthException(Response.UPDATE_APP_MANDATORY_PARAM_MISSING,
							HttpResponseStatus.BAD_REQUEST);
				}
			} catch (JsonParseException e) {
				throw new OAuthException(e, Response.CANNOT_UPDATE_APP, HttpResponseStatus.BAD_REQUEST);
			} catch (JsonMappingException e) {
				throw new OAuthException(e, Response.CANNOT_UPDATE_APP, HttpResponseStatus.BAD_REQUEST);
			} catch (IOException e) {
				throw new OAuthException(e, Response.CANNOT_UPDATE_APP, HttpResponseStatus.BAD_REQUEST);
			}
		} else {
			throw new OAuthException(Response.UNSUPPORTED_MEDIA_TYPE, HttpResponseStatus.BAD_REQUEST);
		}
		return true;
	}

}
