#Single Sign On using ApiFest : OAuth 2.0 implementation

##Overview
Single sign-on (SSO) is a way to control access to multiple related but independent systems, a user only needs to log in once and gains access to all other systems.

##Configurations to run OAuth server
* Edit the following file and configure the database.properties. **/nimbus4-sso/apifest-authenticate/src/main/resources/database.properties** :
  	
           url = jdbc:mysql://localhost:3306/
           dbName = database name
           userName = username
           password = password
  			
* Open the configuration file **/nimbus4-sso/oauth20-server/apifest-oauth20/apifest-oauth.properties** and edit the following
      * custom.classes.jar
* Build each of the below using "mvn clean package install -DskipTests" in exact sequence.
      * apifest-authenticate
      * oauth20-mapping-server
      * oauth20-server
* Go to the location **/nimbus4-sso/oauth20-server/apifest-oauth20** and run the script file "run.sh"
      * For success we get **ApiFest OAuth 2.0 Server started at localhost:3000**
                 
##Registering the admin and api
Nimbus4-api and Nimbus4-admin checks if they are registered with the OAuth server when they run. For this they need to be manually registered
with the OAuth server which is just a one time configuration.

###Curl to register application
curl -i -X POST -H "Content-Type:application/json" -d '{"name":"nimbusApi","description":"desc","scope":"read","redirect_uri":"http://localhost:8090/nimbus4-admin","status":"1","application_details":{"key1":"value"}}' "http://localhost:3000/oauth20/applications"

**Response** : {"client_id":"clientId","client_secret":"clientSecret"}

####where 
* **name** : application name
* **description** : application description
* **redirect_uri** : url for the application

Edit the following in the above for each admin and api : name , description and redirect_uri. 
The above curl return the client_id and client_secret in response. The client_id and client_secret returned for registering api will be used as global_client_id and global_client_secret.

Configure the below in CommonConfig.groovy :

        admin.client.id = //client_id for admin
		global.client.id = //client_id for api
		global.client.secret = //client_secret for api
		oauth.server.url = //url for oauth server 
		
		
##Other useful curls

###Get all registered applications with OAuth server

curl -i -X GET -H "Content-Type:application/json" -d '' "http://localhost:3000/oauth20/applications"

###Validate if client_id registered with OAuth server

curl -i -X GET -H "Content-Type:application/json" -d '' "http://localhost:3000/oauth20/applications/client_id"

Replace client_id in above. For invalid client_id Response : {"error":"Not found"} else response with information is returned.









