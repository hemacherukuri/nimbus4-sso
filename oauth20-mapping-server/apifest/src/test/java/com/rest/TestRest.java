package com.rest;

import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class TestRest {
	
	// calls the greeting without oauth2 security
	@Test
	public void testRestGreeting() {

		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8100/greeting";
		String params = "?name=hemaCheruNew";
		ResponseEntity<String> retValStr = restTemplate.getForEntity(url
				+ params, String.class);

		System.out.println(retValStr);
		ResponseEntity<String> retVal = restTemplate.getForEntity(url + params,
				String.class);
		System.out.println(retVal.getBody());

	}

}
