package com.westcon.oauth2;

import com.apifest.oauth20.api.AuthenticationException;
import com.apifest.oauth20.api.IUserAuthentication;
import com.apifest.oauth20.api.UserDetails;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

public class UserAuthentication implements IUserAuthentication

{
	protected static Logger Logger = LoggerFactory.getLogger(UserAuthentication.class);

	public UserDetails authenticate(String username, String password, HttpRequest arg2) throws AuthenticationException {
		Logger.debug("===================================================");
		Logger.debug("            Validating user credentials            ");
		Logger.debug("===================================================");
		Map<String, String> details = new HashMap<String, String>();
		UserDetails userDetails = new UserDetails(username, details);
		Connection connection = getConnection();
		ResultSet rs = null;
		Statement stmt = null;

		if (connection != null) {
			try {

				stmt = connection.createStatement();
				String email = username.split("~")[0];
				String subdomain = username.split("~")[1].toLowerCase();
				String encryptedPassword = username.split("~")[2];
				List groupIdList = new ArrayList();
				boolean success = false;
				String sql;

				// TODO optimize the query
				sql = "select * from groups where var_group_id = (select tenant_id from group_configuration where subdomain='"
						+ subdomain + "' );";
				rs = stmt.executeQuery(sql);
				while (rs.next()) {
					groupIdList.add(rs.getInt("id"));
				}

				rs = stmt.executeQuery("SELECT * from user where email='" + email + "'");

				while (rs.next()) {
					// todo check for if multiple user exist with same emailid
					int userGroupId = rs.getInt("group_id");
					if (groupIdList.contains(userGroupId)) {
						String shiroEncryptedPassword = new Sha256Hash(password).toHex();
						if (shiroEncryptedPassword.equals(rs.getString("password"))) {
							Logger.debug("User found and password matched");
							success = true;
						} else {
							Logger.info("User found but password did not matched !!!");
						}
					} else {
						Logger.info("User not found");
						return null;
					}
				}

				if (success) {
					return userDetails;
				} else {
					return null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} catch (Throwable e) {
				e.printStackTrace();
			} finally {
				try {
					rs.close();
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					Logger.debug("Error while closing connection !!!!");
					e.printStackTrace();
				}
			}

		} else {
			return null;
		}

		return userDetails;
	}

	// TODO should we get the connection from a pool? hemac

	public static Connection getConnection() {

		String url = props.getProperty("url");
		String dbName = props.getProperty("dbName");
		String userName = props.getProperty("userName");
		String password = props.getProperty("password");

		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url + dbName, userName, password);
			if (connection != null) {
				Logger.debug("Connected to database ... ");
			} else {
				Logger.debug("Failed to make connection !!!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return connection;
	}

	static Properties props = null;
	static String propsFile = "database.properties";

	static {

		InputStream is = UserAuthentication.class.getClassLoader().getResourceAsStream(propsFile);
		props = new Properties();

		try {
			props.load(is);
		} catch (IOException e) {
			Logger.error("Unable to read " + propsFile, e);
			e.printStackTrace();
		}
	}

	public static void main(String[] str) {
		UserAuthentication.getConnection();
	}
}
